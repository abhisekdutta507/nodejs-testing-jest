const lib = require('../lib');
const db = require('../db');

describe('absolute', () => {
  // it() or test() both works same
  test('should return a positive number', () => {
    // throw new Error('something failed');
    let result = lib.absolute(10);
    expect(result).toBe(10);
  });

  // it() or test() both works same
  it('should return a positive number if input is negetive', () => {
    // throw new Error('something failed');
    let result = lib.absolute(-1);
    expect(result).toBe(1);
  });

  // it() or test() both works same
  it('should return 0 if input is 0', () => {
    // throw new Error('something failed');
    let result = lib.absolute(0);
    expect(result).toBe(0);
  });

  it('should return 0 if input is undefined or null', () => {
    // throw new Error('something failed');
    let result = lib.absolute('a');
    expect(result).toBe(0);
  });
});

describe('greet', () => {
  it('should return the greeting message', () => {
    const result = lib.greet('Abhisek');
    expect(result).toMatch(/Abhisek/);
    // or
    // expect(result).toContain('Hi Abhisek!');
  });
});

describe('getCurrencies', () => {
  it('should return the currencies', () => {
    const result = lib.getCurrencies();

    // Too general
    expect(result).toBeDefined();
    expect(result).not.toBeNull();

    // Too specific
    expect(result[0]).toBe('USD');
    expect(result[1]).toBe('INR');
    expect(result.length).toBe(2);

    // Proper way
    expect(result).toContain('USD');
    expect(result).toContain('INR');

    // Ideal way
    // https://jestjs.io/docs/en/expect#expectarraycontainingarray
    // use toEqual to compare array and objects
    expect(result).toEqual(expect.arrayContaining(['INR', 'USD']))
  });
});

describe('getProduct', () => {
  it('should return the product details', () => {
    const result = lib.getProduct('12865309');
    // use toEqual to compare array and objects
    expect(result).toEqual({ id: '12865309', stock: 10 });
    // or
    expect(result).toMatchObject({ id: '12865309' });
    // or
    // data type should match
    expect(result).toHaveProperty('id', '12865309');
  });
});

describe('registerUser', () => {
  // exception handling
  it('should throw an error when username not given', () => {
    const args = [null, undefined, NaN, '', 0, false];
    args.forEach((a) => {
      expect(() => { lib.registerUser(a); }).toThrow();
    });
  });

  it('should return user object when username is given', () => {
    const result = lib.registerUser('AbhisekDutta01');
    expect(result).toMatchObject({ username: 'AbhisekDutta01' });
    expect(result.id).toBeDefined();
  });
});

describe('fizzBuzz', () => {
  it('should throw error if input is not a number', () => {
    const args = [null, undefined, NaN, '', true, false, {}];
    args.forEach((a) => {
      expect(() => { lib.fizzBuzz(a) }).toThrow();
    });
  });

  it('should return FizzBuzz if input is divisible by 3 and 5 both', () => {
    const args = [15, 30];
    args.forEach((a) => {
      const result = lib.fizzBuzz(a);
      expect(result).toBe('FizzBuzz');
    });
  });

  it('should return Fizz if input is divisible by 3 and not divisible by 5', () => {
    const args = [3, 6, 9, 12];
    args.forEach((a) => {
      const result = lib.fizzBuzz(a);
      expect(result).toBe('Fizz');
    });
  });

  it('should return Buzz if input is divisible by 5 and not divisible by 3', () => {
    const args = [5, 10, 20];
    args.forEach((a) => {
      const result = lib.fizzBuzz(a);
      expect(result).toBe('Buzz');
    });
  });

  it('should return the input if input not divisible by 3 and 5 both', () => {
    const args = [4, 7, 11];
    args.forEach((a) => {
      const result = lib.fizzBuzz(a);
      expect(result).toBe(a);
    });
  });
});

describe('applyDiscount', () => {
  it('should throw error if the customer does not exists', () => {
    const order = { customerId: 2, totalPrice: 10 };
    expect(() => { lib.applyDiscount(order); }).toThrow();
  });

  it('should apply 10% discount if the customer has more than 10 points', () => {
    const order = { customerId: 1, totalPrice: 10 };
    lib.applyDiscount(order);
    expect(order.totalPrice).toBe(9);
  });

  it('should not be any discount if the customer has less than 10 points', () => {
    const order = { customerId: 4, totalPrice: 20 };
    lib.applyDiscount(order);
    expect(order.totalPrice).toBe(20);
  });
});

describe('getCustomer', () => {
  describe('async function', () => {
    it('should return the customer with promise', async () => {
      const result = await db.getCustomer(1);
      expect(result).toMatchObject({ id: 1 });
    });
  });

  describe('sync function', () => {
    it('should return mock response', () => {
      // skip an async call using jest mock
      db.getCustomer = jest.fn().mockReturnValue(5);

      const result = db.getCustomer();
      expect(db.getCustomer).toHaveBeenCalled();
      expect(result).toBe(5);
    });

    it('should be overriden with mock function', () => {
      // skip an async call using jest mock
      db.getCustomer = jest.fn();

      db.getCustomer();
      expect(db.getCustomer).toHaveBeenCalled();
      expect(db.getCustomer.mock.calls[0][0]).toBe(undefined);
    });
  });
});
