const request = require('supertest');
let server;

jest.mock('../models/db', () => ({
  getCustomer: async (id) => {
    return new Promise(next => {
      setTimeout(() => {
        next({ id, points: 10 });
      }, 500);
    })
  }
}))

describe('/api/customer/:id', () => {
  beforeEach(() => { server = require('../index'); });
  afterEach(() => { server.close(); });

  describe('GET /', () => {
    it('should return error if the customer does not exists', async () => {
      const response = await request(server).get('/api/customer/2');
      const customer = JSON.parse(response.text);
      expect(customer.error).toBe(true);
    });

    it('should return the customer details if exists', async () => {
      const response = await request(server).get('/api/customer/1');
      const customer = JSON.parse(response.text);
      expect(customer.error).toBe(false);
    });
  });
});