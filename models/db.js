const customers = [
  { id: 1, points: 11 },
  { id: 4, points: 9 }
];

module.exports.getCustomerSync = (id) => {
  const customer = customers.find(c => (c.id === id));
  return customer;
}

module.exports.getCustomer = async (id) => {
  return new Promise(next => {
    setTimeout(() => {
      const customer = customers.find(c => (c.id === id));
      next(customer);
    }, 500);
  });
}