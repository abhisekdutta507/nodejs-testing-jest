const db = require('./models/db');

module.exports.absolute = (n) => {
  if (n > 0) return n;
  if (n < 0) return -n;
  return 0;
}

module.exports.greet = (username) => {
  return `Welcome ${username}!`;
}

module.exports.getCurrencies = () => {
  return ['USD', 'INR'];
}

module.exports.getProduct = (id) => {
  return { id, stock: 10 };
}

module.exports.registerUser = (username) => {
  if (!username) throw new Error('username is required.');
  return { id: new Date().getTime(), username };
}

module.exports.fizzBuzz = (input) => {
  if (typeof input !== 'number' || isNaN(input))
    throw new Error('Input should be a number.');

  if ((input % 3 === 0) && (input % 5 === 0))
    return 'FizzBuzz';

  if (input % 3 === 0)
    return 'Fizz';

  if (input % 5 === 0)
    return 'Buzz';

  return input;
}

module.exports.applyDiscount = (order) => {
  const customer = db.getCustomerSync(order.customerId);

  if (!customer) throw new Error('customerId does not exists');

  if (customer.points > 10) {
    order.totalPrice *= 0.9;
  }
}