// unit testing
// integration testing
// end-to-end testing
const express = require('express');
const app = express();
const api = express.Router();
const db = require('./models/db');

const routes = () => {
  // route parameter
  api.get('/customer/:id', async (req, res) => {
    const customer = await db.getCustomer(Number(req.params.id));

    if (!customer) {
      return res.status(200).send({ error: true, message: 'customer does not exists', data: {} });
    }

    return res.status(200).send({ error: false, data: customer });
  });

  return api;
};

app.use('/api', routes());

const server = app.listen(3000, () => { console.log('listening to port 3000'); });

module.exports = server;